# Cesium SDK

#### 介绍
基于 Cesium 开发三维 WebGIS，封装一些常用的方法以及解决方案。

#### 方法说明

1. 2019-08-06 | getTilesetCenter | 获取模型原始中心位置
2. 2019-08-06 | addModel | 添加倾斜摄影模型
3. 2019-08-06 | toScreen | 获取经纬度在屏幕上的位置
4. 2019-08-06 | getHeight | 获取当前视角高度
5. 2019-08-06 | getCenter | 获取当前中心点坐标
6. 2019-08-06 | getClickPos | 注册/注销 获取当前点击的经纬度坐标
7. 2019-08-06 | addMaker | 添加标记
8. 2019-08-06 | fly2Point | 飞行至指定的经纬度
9. 2019-08-08 | addLayer | 添加图层
10. 2019-08-08 | removeLayer | 移除图层
11. 2019-08-08 | removeAllLayer | 移除所有图层
12. 2019-08-08 | showLayer | 显示图层
13. 2019-08-08 | hideLayer | 隐藏图层
14. 2019-08-08 | hideAllLayer | 隐藏所有图层
15. 2019-09-04 | createViewer | 创建场景
16. 2019-09-04 | addGif | 添加动态标记层
17. 2019-09-04 | removeGif | 移除动态标记图层
18. 2019-09-04 | lookAround | 围绕中心点旋转
19. 2019-09-04 | removeLookAround | 注销中心点旋转



#### 参与贡献

Dawud | Dawudcn@icloud.com